<?php

namespace App\Repositories;
use App\Interfaces\AttributeRepositoryInterface;
use App\Models\Attribute;

/**
 * implement Attribute interface
 */
 class AttributeRepository implements AttributeRepositoryInterface
 {
     public function createAttribute(array $attributeDetails)
     {
         return Attribute::create($attributeDetails);
     }

     public function updateAttribute ($attributeId, array $newAttributeDetails)
     {
        return Attribute::whereId($attributeId)->update($newAttributeDetails);
     }

     public function getAllAttributes()
     {
         return Attribute::paginate(25);
     }

     public function getAttributeById($attributeId)
     {
         return Attribute::findOrFail($attributeId);
     }

     public function deleteAttribute($attributeId)
     {
        Attribute::destroy($attributeId);
     }
 }