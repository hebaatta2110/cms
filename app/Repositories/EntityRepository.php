<?php

namespace App\Repositories;
use App\Interfaces\EntityRepositoryInterface;
use App\Models\Entity;

/**
 * implement entity interface
 */
 class EntityRepository implements EntityRepositoryInterface
 {
     public function createEntity(array $entityDetails)
     {
         return Entity::create($entityDetails);
     }

     public function updateEntity ($entityId, array $newentityDetails)
     {
        return Entity::whereId($entityId)->update($newentityDetails);
     }

     public function getAllEntities()
     {
         return Entity::paginate(25);
     }

     public function getEntityById($entityId)
     {
         return Entity::findOrFail($entityId);
     }

     public function deleteEntity($entityId)
     {
       Entity::destroy($entityId);
     }
 }