<?php

namespace App\Repositories;
use App\Interfaces\RecordRepositoryInterface;
use App\Models\Entity;
use App\Models\Attribute;
use App\Models\Record;
 

/**
 * implement Opertors interface
 */
 class RecordRepository implements RecordRepositoryInterface
 {
    public function createRecord(array $recordDetails)
    {
       return Record::create($recordDetails);
    }
    public function getAllRecords($entityId)
    {
        
       return Record::where('entity_id',$entityId)->paginate(25);
      
    }

    public function getRecordById($recordId)
    {
        return Record::findOrFail($recordId);
    }

 }