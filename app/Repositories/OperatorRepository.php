<?php

namespace App\Repositories;
use App\Interfaces\OperatorRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Arr;


/**
 * implement Opertors interface
 */
 class OperatorRepository implements OperatorRepositoryInterface
 {
   
    public function getAllOperators()
    {
        return User::where('role','operator')->paginate(25);
    }
    public function getOperatorById($operatorId)
    {
        return User::findOrFail($operatorId);
    }
    public function deleteOperator($operatorId)
    {
        return User::destroy($operatorId);

    }
    public function createOperator(array $operatorDetails)
    {
        return User::create(array_merge($operatorDetails,['password' => bcrypt($operatorDetails['password'])]));
    }

    public  function updateOperator($operatorId, array $newOperatorDetails)
    {
        if (Arr::has($newOperatorDetails, 'password')) {
            return User::whereId($operatorId)
            ->update(array_merge($newOperatorDetails,['password' => bcrypt($newOperatorDetails['password'])]));
        }else{
            return User::whereId($operatorId)->update($newOperatorDetails);
        }
      
    }

 }