<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    protected $fillable = ['entity_name'];

    //many to many relation with attributes
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class,'entity_attributes')->withPivot('value');
    }
}
