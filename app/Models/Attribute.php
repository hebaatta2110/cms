<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $fillable = ['attribute_name','attribute_type','validation_rules','required'];

    //many to many relation with attributes
 
    public function entities()
    {
        return $this->belongsToMany(Entity::class,'entity_attributes')->withPivot('value');
    }
    public function records()
    {
        return $this->belongsToMany(Record::class)->withPivot('value');
    }

}
