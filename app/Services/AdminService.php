<?php

namespace App\Services;
use App\Models\Entity;
use Illuminate\Support\Facades\Validator;


class AdminService
{
    public function save($entityId, $attributeIds)
    {
     $entity = Entity::find($entityId);
     // attach Atrributes to entity
     $entity->attributes()->attach($attributeIds);
     return $entity->attributes()->get();
    }

     
}