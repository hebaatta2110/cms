<?php

namespace App\Http\Controllers;

use App\Interfaces\AttributeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\ValidationRules;

class AttributeController extends Controller
{
    //
    private AttributeRepositoryInterface $attributeRepository; 
     
    public function __construct(AttributeRepositoryInterface $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    public function index()
    {
        return response([
            'data' => $this->attributeRepository->getAllAttributes()
        ]);
    }

    public function show($attributeId)
    {
        $validator = Validator::make(['id' => $attributeId], [
        'id' => 'required|exists:attributes,id',
       ]);

       if ($validator->fails()) {
        return response()->json(['errors' => $validator->errors()], 422);
       }
       return response([
            'data' => $this->attributeRepository->getAttributeById($attributeId)
        ]);
    }
    public function store(Request $request)
    { 
        $validator = Validator::make($request->all(), ValidationRules::createAttributeRules());
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }else{ 
             $attributeDetails = $validator->validated();
        }
      
        return response([
            'message' => 'Data added successfully',
            'data' => $this->attributeRepository->createAttribute($attributeDetails)
        ]);
    }
     
    public function update($attributeId ,Request $request)
    {
        $validator = Validator::make($request->all(), ValidationRules::updateAttributeRules());
           if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
           }
        $newAttributeDetails = $validator->validated();
        $this->attributeRepository->updateAttribute($attributeId , $newAttributeDetails);
        return response([
            'message' => 'Data updated successfully',
            'data' => $this->attributeRepository->getAttributeById($attributeId)

        ]);
    }
    public function destroy($attributeId)
    {
        $validator = Validator::make(['id' => $attributeId], [
            'id' => 'required|exists:attributes,id',
           ]);
    
           if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
           }
       $this->attributeRepository->deleteAttribute($attributeId);
       return response([
        'message' => 'Data deleted successfully',
        'data' => $this->attributeRepository->getAllAttributes()
        ]);
    }

}
