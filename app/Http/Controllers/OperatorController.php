<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Entity;
use App\Models\Record;
use App\Models\Attribute;

use Illuminate\Support\Facades\Validator;
use App\ValidationRules;
use App\Interfaces\RecordRepositoryInterface;

class OperatorController extends Controller
{
   
    private RecordRepositoryInterface $recordRepository;

    public  function __construct(RecordRepositoryInterface $recordRepository)
    {
        return $this->recordRepository = $recordRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($entityId)
    {
        $validator = Validator::make(['entity_id' => $entityId], ['entity_id' => 'required|exists:entities,id',]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }
        return response()->json([
            'data' => $this->recordRepository->getAllRecords($entityId)
          ]);
    }

     /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $attribute = Attribute::find($request->attribute_id);
        $validator = Validator::make($request->all(), ValidationRules::createRecordRules($attribute));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }else{ 
             $recordDetails = $validator->validated();
        }
     
        $this->recordRepository->createRecord($recordDetails);
        return response([
            'message' => 'Data added successfully',
            'data' => $this->recordRepository->createRecord($recordDetails)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'data' => $this->recordRepository->getRecordById($id)
          ]);

    }

}
