<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\OperatorRepositoryInterface;
use Illuminate\Http\Response;
use App\Services\AdminService;
use Illuminate\Support\Facades\Validator;
use App\ValidationRules;


class AdminController extends Controller
{
    //
    private OperatorRepositoryInterface $operatorRepository;

    public function __construct(OperatorRepositoryInterface $operatorRepository)
    {
       return $this->operatorRepository = $operatorRepository;
    }
    
    public function index()
    {
        return response([
            'data' => $this->operatorRepository->getAllOperators()
        ]);
    }

    public function show($operatorId)
    {
        $validator = Validator::make(['id' => $operatorId], [
            'id' => 'required|exists:users,id',
        ]);
    
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);

        }
        return response([
            'data' => $this->operatorRepository->getOperatorById($operatorId)
        ]);
    }
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ValidationRules::createOperatorRules());
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }else{ 
             $operatorDetails = $validator->validated();
        }
         return response([
        'message' => 'Data added successfully',
        'data' => $this->operatorRepository->createOperator($operatorDetails)
          ]);
    }

    public function update($operatorId, Request $request)
    {  
        $validator = Validator::make($request->all(), ValidationRules::updateOperatorRules());
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }else{ 
             $newOperatorDetails = $validator->validated();
        }
     
        $this->operatorRepository->updateOperator($operatorId,$newOperatorDetails);
        return response([
            'message' => 'Data updated successfully',
            'data' => $this->operatorRepository->getOperatorById($operatorId)

        ]);
    }

    public function destroy($operatorId)
    {
        $validator = Validator::make(['id' => $operatorId], [
            'id' => 'required|exists:users,id',
        ]);
    
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);

        }
       $this->operatorRepository->deleteOperator($operatorId);
       return response([
        'message' => 'Data deleted successfully',
        'data' => $this->operatorRepository->getAllOperators()
       ]);

    }

    public function assignAttribute(Request $request)
    {
        $validator = Validator::make($request->all(), ValidationRules::assignAttributeRules());

         if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);

        }
      $data = AdminService::save($request->entity_id ,$request->attribute_id);
      return response([
        'message' => 'Data added successfully',
        'data' => $data
       ]);
    }

}
