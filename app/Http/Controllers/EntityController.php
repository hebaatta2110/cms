<?php

namespace App\Http\Controllers;
use App\Interfaces\EntityRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\ValidationRules;

class EntityController extends Controller
{
    //
    private EntityRepositoryInterface $entityRepository;

    public function __construct(EntityRepositoryInterface $entityRepository)
    {
       $this->entityRepository = $entityRepository;
    }

    public function index()
    {
        return response()->json([
          'data' => $this->entityRepository->getAllEntities()
        ]);
    }

    public function show($entityId)
    {
        $validator = Validator::make(['id' => $entityId], ['id' => 'required|exists:entities,id',]);
    
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }
        return response([
            'data' => $this->entityRepository->getEntityById($entityId)
        ]);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ValidationRules::createEntityRules());
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }else{ 
             $entityDetails = $validator->validated();
        }
      
       return response([
        'message' => 'Data added successfully',
        'data' => $this->entityRepository->createEntity($entityDetails)
       ]);
    }

    public function update ($entityId, Request $request)
    {
        $validator = Validator::make($request->all(), ValidationRules::updateEntityRules());
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }else{ 
             $newentityDetails = $validator->validated();
        }
        
        $this->entityRepository->updateEntity($entityId,$newentityDetails);
        return response([
            'message' => 'Data updated successfully',
            'data' => $this->entityRepository->getEntityById($entityId)
        ]);
    }

    public function destroy($entityId)
    {
        $validator = Validator::make(['id' => $entityId], ['id' => 'required|exists:entities,id', ]);
    
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }
        $this->entityRepository->deleteEntity($entityId);
        return response([
            'message' => 'Data deleted successfully',
            'data' => $this->entityRepository->getAllEntities()
        ]);
    }

}
