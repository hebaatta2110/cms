<?php

namespace App;
use Illuminate\Validation\Rule;
use App\Models\Attribute;

class ValidationRules
{
    public static function createOperatorRules()
    {
        return [
            'name' => 'required|string|unique:users|between:2,10',
            'email' => 'required|string|email|max:50|unique:users',
            'password' => 'required|string|min:8',
            'role' => 'required|string'
        ];
    }
    public static function updateOperatorRules()
    {
    return [
        'name' => 'string',
        'email' => 'email|unique:users,email',
        'password' => 'string|min:8',
    ];
   }

   public static function assignAttributeRules()
    {
      return [
        'entity_id' => 'required|exists:entities,id',
        'attribute_id' => 'required|exists:attributes,id'
        
      ];
   }

   public static function createAttributeRules()
   {
     return [
        'attribute_name' =>'required|string',
        'attribute_type' => 'required|string|in:Integer,float,string,boolean,date,time,datetime',
        'validation_rules' => 'required|string',
        'required' =>'required|boolean'
       
     ];
  }
    public static function updateAttributeRules()
     {
    return [
       'attribute_name' =>'string',
       'attribute_type' => 'string|in:Integer,float,string,boolean,date,time,datetime',
       'validation_rules' => 'string',
       'required' =>'boolean'
      
    ];
     }
     public static function createEntityRules()
     {
        return [
         'entity_name' =>'required|string',
      ];
    }
    public static function updateEntityRules()
    {
       return [
        'entity_name' =>'string',
     ];
    }

    public static function createRecordRules($attribute)
    {
         return [
            'entity_id' => 'required|exists:entities,id',
            'attribute_id' => 'required|exists:attributes,id',
            'value' =>$attribute->attribute_type,
         ];

    }

 
}
