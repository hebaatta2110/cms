<?php

namespace App\Providers;

use App\Repositories\EntityRepository;
use App\Interfaces\EntityRepositoryInterface;
use App\Repositories\AttributeRepository;
use App\Interfaces\AttributeRepositoryInterface;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\OperatorRepositoryInterface;
use App\Repositories\OperatorRepository;
use App\Interfaces\RecordRepositoryInterface;
use App\Repositories\RecordRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(EntityRepositoryInterface::class, EntityRepository::class);
        $this->app->bind(AttributeRepositoryInterface::class, AttributeRepository::class);
        $this->app->bind(OperatorRepositoryInterface::class, OperatorRepository::class);
        $this->app->bind(RecordRepositoryInterface::class, RecordRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
