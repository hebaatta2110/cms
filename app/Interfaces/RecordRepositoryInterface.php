<?php
namespace App\Interfaces;

interface RecordRepositoryInterface
{
 // add all methods 
  public function createRecord(array $recordDetails);
  public function getAllRecords($entityId);
  public function getRecordById($recordId);

}
