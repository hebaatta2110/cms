<?php
namespace App\Interfaces;

interface OperatorRepositoryInterface
{
 // add all methods 
  public function createOperator(array $operatorDetails);
  public function updateOperator ($operatorId, array $newOperatorDetails);
  public function getAllOperators();
  public function getOperatorById($operatorId);
  public function deleteOperator($operatorId);

}
