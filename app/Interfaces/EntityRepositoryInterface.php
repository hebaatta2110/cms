<?php
namespace App\Interfaces;

interface EntityRepositoryInterface
{
 // add all methods 
  public function createEntity(array $entityDetails);
  public function updateEntity ($entityId, array $newentityDetails);
  public function getAllEntities();
  public function getEntityById($entityId);
  public function deleteEntity($entityId);

}
