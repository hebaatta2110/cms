<?php
namespace App\Interfaces;

interface AttributeRepositoryInterface
{
 // add all methods 
  public function createAttribute(array $attributeDetails);
  public function updateAttribute ($attributeId, array $newAttributeDetails);
  public function getAllAttributes();
  public function getAttributeById($attributeId);
  public function deleteAttribute($attributeId);

}
