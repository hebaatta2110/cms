<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EntityController;
use App\Http\Controllers\AttributeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\OperatorController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api','prefix' => 'auth'], function($router){
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);
});
Route::group(['middleware' => ['auth:api','isAdmin'],'prefix' => 'admin'], function($router){
    //routes of entities
    Route::get('entity', [EntityController::class, 'index']);
    Route::get('entity/{id}', [EntityController::class, 'show']);
    Route::post('new_entity', [EntityController::class, 'store']);
    Route::post('update_entity/{id}', [EntityController::class, 'update']);
    Route::delete('delete_entity/{id}', [EntityController::class, 'destroy']);
    
    //routes of Attributes
    Route::get('attribute', [AttributeController::class, 'index']);
    Route::get('attribute/{id}', [AttributeController::class, 'show']);
    Route::post('new_attribute', [AttributeController::class, 'store']);
    Route::post('update_attribute/{id}', [AttributeController::class, 'update']);
    Route::delete('delete_attribute/{id}', [AttributeController::class, 'destroy']);
    
    //routes for operators
    Route::get('operator', [AdminController::class, 'index']);
    Route::get('operator/{id}', [AdminController::class, 'show']);
    Route::post('new_operator', [AdminController::class, 'store']);
    Route::post('update_operator/{id}', [AdminController::class, 'update']);
    Route::delete('delete_operator/{id}', [AdminController::class, 'destroy']);
    Route::post('assign_attribute', [AdminController::class, 'assignAttribute']);

});
Route::group(['middleware' => ['auth:api','isOperator'],'prefix' => 'operator'], function($router){
    Route::post('new_record', [OperatorController::class, 'store']);
    Route::get('records/{entityId}', [OperatorController::class, 'index']);
    Route::get('record/{id}', [OperatorController::class, 'show']);
});